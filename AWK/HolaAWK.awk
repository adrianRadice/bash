#!/usr/bin/awk -f
BEGIN{
	  RS=":"
			FS="|"
			ORS_OLD=ORS
			OFS_OLD=OFS
			ORS=":"
			OFS="|"
			cont=0
			res["M"]=0
			res["F"]=0
			res["PROM"]=0
			res["APRO"]=0
			res["REC"]=0
}
$6 == 1 { 
			cont++
			res[$3>=7 && $4>=7?"PROM":$3>=4&&$4>=4?"APRO":"REC"]++
			print cont,$1,$2 > "alumnosActivos"
			res[$5]++
}
END{
	   ORS=ORS_OLD
	   OFS=OFS_OLD
    print "Total Alumnos Activos: ", cont
    print "|-> Mujeres: ", res["F"]
    print "|-> Hombres: ", res["M"]
				print "|-> Promocionados: ", res["PROM"]
				print "|-> Aprobados: ", res["APRO"]
				print "|-> Recursa: ", res["REC"]
				
}