# ¿Cómo entregar el Práctico?

* Se debe entregar una Carátula impresa por TP. ***COMPLETAR CAMPOS SOLICITADOS Y RESPETAR LO INDICADO ENTRE PARÉNTESIS***
* Archivo comprimido con el siguiente contenido:
* Una carpeta por ejercicio, su titulo será EjX
* Es obligatorio que cada ejercicio incluya un lote de prueba propio. El mismo se compondrá  de la entrada y un script disparador para la ejecución  del mismo.

### *NOTA: asegurarse que los script a entregar funcionen en el laboratorio. ``OS laboratorio Lubuntu 18.0.4``*

# ¿Que necesitamos para realizar el tp?
Para realizar el tp vamos a requerir una instancia de alguna distribución de GNU/Linux.

*Recordar: los script a entregar deben funcionar en el laboratorio. La distribución de GNU/Linux utilizada en los laboratorio es ``Lubuntu 18.0.4``*

# ¿Como ejecutamos una instancia de linux?
* Instalamos el OS nativamente en una computadora
* Instalar el OS en una VM. (Alternativa recomendada)

# ¿Como construir una VM?
La catedra recomienda para introducirse en el mundo de la virtualización utilizar Oracle Virtual Box. Usted tiene de todas formas la libertad de usar otra alternativas de virtualización como VMware, Docker, Parallels, etc.

# ¿Como utilizar Virtual Box?
1.  Descargar virtual box. https://www.virtualbox.org/wiki/Downloads



# Introducción a BASH


# ¿Que es bash?

* Es el ``Interprete de Comando (shell)`` generalmente por defecto en las distribuciones GNU/Linux.  
* Bash a igual que cualquier otro tipo de shell (Ejemplo sh) ``funciona como interfaz entre el kernel y los usuarios.`` 
* ``Al interactuar con Bash utilizamos un lenguaje dinámico, orientado a texto.``




# IF
* Estructura: 

```sh
if <condition> ; then 
    <commands> 
elif <condition>; then
    <commands> 
else
    <commands> 
fi
```

* ¿Como funciona? Evalúa la salida de un comando.
 * La salida de un comando en el mundo Linux es:  0 => True ; >0 => False

*Les parecerá  raro que True es 0. Él  porque es porque como se indico es la salida de un comando lo que se está  evaluando, y los errores son los que se enumeran.
Recordemos un poco de C*
```sh
#include<stdio.h>
int main(){
    int i;
    printf("Ingrese un numero positivo para continuar: ");
    scanf("%d",&i);
    if(i<0)
        return 1; //EXIT_FAILURE;
    ....
    return 0; //EXIT_SUCCESS;
}
```

* Como escribimos las condiciones
Forma larga: 

```sh
 if test <comparacion>; then
 fi
```

*test si la condición  es verdadera retornara 0 y sino >0, es el comando evaluado*

Una manera más  corta, es remplazar test por **[]**
```sh
 if test $a -ge 6; then
 fi
 # es equivalente a 
 if [ $a -ge 6 ]; then
 fi
```
Notar que se emplea los mismo condicionales de PowerShell 

| Operador |Nombre| Ejemplo | Equivalente C | Uso | 
| -------- | -------- | -------- | :--------: | -------- |
| **-eq**   |**Eq**uality| 2 -eq 2   | == | Compara si dos objetos son iguales
| **-ne**   |**N**ot **E**qual| 4 -ne 2   | != | Compara si dos objetos son distintos
| **-lt**   |**L**ess **T**han| 1 -lt 2   | <  | Compara si un objeto es menor a otro
| **-le**   |**G**reater Than or **E**qual To| 1 -le 5   | <= | Compara si un objeto es menor igual a otro
| **-gt**   |**G**reater **T**han| 2 -gt 1   | >  | Compara si un objeto es mayor a otro
| **-ge**   |**L**ess Than or **E**qual To| 2 -ge 1   | >= | Compara si un objeto es mayor igual a otro


Los operadores and y or son -a y -o.
```sh
 if [ $a -ge 6  -a $a -le 9 ]; then
 fi
```

Bash admite la sintaxis de doble paréntesis  para comparar números  enteros únicamente, usando los operadores == >= && || ... en lugar de los -eq ...
Analogía 
 * (()) => let
 * [] => test

```sh
 if test $a -ge 6; then
 fi
 # es equivalente a 
 if [ $a -ge 6 ]; then
 fi
 # es equivalente a 
 if (( $a == 6 )); then
 fi
```
Negar una condición  implica el operador ! 
```sh
 if [ ! $a -ge 6 ]; then
 fi
```

Otra sintaxis es la de doble corchete y la misma habilita el uso de regex.
Algunas consideraciones para esta última  sintaxis

```sh
 if [[ "$var" == *cadena* ]]; then 
 fi
 # NO ES EQUIVALENTE A, EL PRIMER CASO EVALUA LA QUE LA VARIABLE CONTENGA LA CADENA, MIENTRAS QUE EL SEGUNDO QUE SEA IGUAL
 if [ "$var" -eq *cadena* ]; then 
 fi
```

AlGUNOS OPERADORES ESPECIALES

| Operador | Uso | 
| -------- | -------- | -------- | :--------: | -------- |
| [ -d FILE ] | True si FILE existe y es un directorio
| [ -e FILE ] | True si existe FILE
| [ -f FILE ] | True si existe FILE y es un archivo regular
| [ -h FILE ] | True si file existe y es un symbolic link.
| [ -p FILE ] | True si el archivo existe y es un pipe.
| [ -r FILE ] | True si el archivo existe y tiene permiso de lectura
| [ -s FILE ] | True si el archivo existe y su longitud es mayor a 0.
| [ -w FILE ] | True si el archivo existe y tiene permiso de lectura.
| [ -x FILE ] | True si el archivo existe y tiene permiso de ejecucion.
| [ -z FILE ] | True si la cadena es vacia.
| [ -n FILE ] | True si la cadena no es vacia.


### Regex y operador ~=
* `.`  Significa cualquier caracter.
* `^`  Indica el principio de una línea.
* `$`  Indica el final de una línea.
* `*`  Indica cero o más repeticiones del caracter anterior.
* `+`  Indica una o más repeticiones del caracter anterior.
* `\<` Indica el comienzo de una palabra.
* `\>` Indica el final de una palabra.
* `\`  Caracter de escape. Da significado literal a un metacaracter.
* `[ ]`  Uno cualquiera de los caracteres entre los corchetes. Ej: [A-Z] (desde A hasta Z).
* `[^ ]` Cualquier caracter distinto de los que figuran entre corchetes: Ej: [^A-Z].
* `{ }`  Nos permiten indicar el número de repeticiones del patrón anterior que deben darse.
* `|`  Nos permite indicar caracteres alternativos: Ej: (^|[?&])
* `( )`  Nos permiten agrupar patrones. Ej: ([0-9A-F]+:)+


```sh
 if [[ $var ~= ^[0-9]+$ ]]; then 
    echo numero
 fi
```


# Ciclos

### while: 
* Ejecuta una seria de comandos mientras que una determinada condición  sea cumpla.
* Sintax

```sh
 while [ <condicion> ]; do
  # codigo
 done
```

### until: 
* Ejecuta una serie de comandos hasta que una determinada condición  se cumpla.
* Sintax

```sh
 until [ <condicion> ]; do
  # codigo
 done
```

### for:
* Ejecuta una serie de comandos un numero determinado de veces.
* Sintax: tenemos varias formas:
 * Equivalente a c:

```sh
for (( ; ; )) ## SI NO COMPLETAMOS ES UN LOOP INFINITO
do  
   #codigo
done
```


 *  Iterando sobre resultado de un comando
 *  
 

```sh
for OUTPUT in $(Linux-Or-Unix-Command)
do  
   #codigo
done
```


 *  Iterando sobre listas
 *  
 

```sh
# Lista numerica explicita
for OUTPUT in 1 2 5 6 78 9
do  
   #codigo
done
# Lista numerica generada explicita
for OUTPUT  in {0..10..2} #{comienzo..fin..incremento} el incremento por defecto es 1 y puede no especificarse
do  
   #codigo
done
# Iterando sobre un script
IFSVIEJO=$IFS
IFS='-' ## DELIMITADOR DE CAMPOS QUE POR DEFAULT ES EL ESPACIO
for OUTPUT in  "HOLA-MUNDO"
do  
   #codigo
done
IFS=$IFSVIEJO
```


# Case y Select
* case: Ejecuta una o varias listas de comandos dependiendo del valor de una variable.
* select: Permite seleccionar al usuario una opcion de una lista de opciones en un menu.

# Arreglos
### Características
* No tienen un límite  fijo de tamaño (Solo el espacio disponible en la memoria lo limita).
* Se indexan a partir del 0. 
* No se almacenan contiguamente en memoria Arreglos dispersos.
* Son Unidimensionales.

### Tipos
* Indexados (a)
* Asociativos (A)

### Declaración
* Crear un array vacio, en forma indirecta
```sh
vec=() 
```

* Crear un array inicializado, en forma indirecta
```sh
vec=('chau',123,'UNLAM')
```

* Crear un array inicializado, en forma indirecta, indicando los indices
```sh
vec=([0]='chau' [3]=123 [1]='UNLAM')
```

* Crear un array vacío, en forma explicita 
```sh
declare -a vecIndexado
declare -a vecAsociativo
```

* Crear un array en forma explícita  y inicializado
```sh
declare -a vec=('hola','tu',123.12)
```
*Nota: Se crea un vector de 3 elementos [0]=>'hola';[1]=>'tu';[2]=>123.12*

* Crear un array a partir  de otro
```sh
vecCopia=("${vecOriginal[*]}")
vecCopiaConcat=("${vec1[*]}" "${vec2[*]}" )
vecCopiaEditado=("${vecOriginal[*]/valorAremplzar/nuevoValor}")
```

* Convertir una variable en array
```sh
var=2
var[1]=21
```
*Nota: Cualquier variable se puede considerar como un arreglo de un elemento*

* Agregar un elemento a un array
```sh
vec+=(<lista de nuevos elementos>)
```

### Limpiar arreglo

* Liberar espacio array
```sh
unset -v array
```

* Liberar un elemento
```sh
unset -v array[1]
```

### Acceder a elementos de un arreglo
| vec = ( | a |, b |, c |, d | )|  
| :--------: | :--------: | :--------: | :--------: | :--------: | :--------: |
|Index +| 0 | 1 | 2 | 3 | 
|Index -| -4 | -3 | -2 | -1 | 

```sh
# Indices positivos
for ((i=0;i<4;i++)); do
    echo ${array[$i]}
done
# Indices negativos
for ((i=-1;i>=-4;i--)); do
    echo ${array[$i]}
done
# Mostrar todos los elementos
echo ${array[*]}
echo ${array[@]}

#Acceder a un rango de elementos
echo ${array[@]:1:2}
```

* Obtener longitud del array
```sh
echo ${#array[@]}
```

* Obtener indices del array
```sh
echo ${!array[*]}
```

* Decir cuál  de las dos siguientes formas es la correcta para incrementar en uno los elementos de un array* 

```sh
for item in ${array[$i]}; di
    (( item = $item + 1 ))
done

for ((i=-1;i>=-4;i--)); do
    (( array[$i] = ${array[$i]} +1 ))
done
```

# Señales

* Enviar una señal
```sh
kill [señal] [PID]
```

* Consultar listado de señales
```sh
kill -l
```

* Consultar información  de una señal en particular
```sh
man [numeroSeñal] signal
```

* Capturar una señal
```sh
trap [funcion] [listado señales separados por espacio]
```

* Restablecer comportamiento al capturar una señal
```sh
trap [señal a restablecer]
```

# Demonios
Un demonio podría  entenderse como un proceso ejecutándose  en segundo plano.
Para ejecutar un script en segundo plano, debemos utilizar el operador &

```sh
./miScript.sh &
```

Un demonio básicamente podríamos entenderlo como un loop infinito ejecutando un conjunto de tareas.

Pensemos la estructura básica para hacer un demonio:

```sh
#!/bin/bash
echo PID: $$
while true
do
  # Grupo de tareas que queremos que se ejecuten.
done
```

El código  anterior es suficiente para crear un proceso demonio. Como deberíamos  ejecutarlo

```sh
./demonio.sh &
```

Para detener el mismo 

```sh
KILL SEÑAL PID
```

Ahora bien, observemos que, si dejamos el demonio como esta, sería poco eficiente porque estaría consumiendo casi todo el tiempo del cpu, para ello tenemos dos caminos a optar, la elección del mismo
dependerá si el demonio responderá rápidamente o no a un estímulo externo.

* Si el tiempo de respuesta no es crítico  podemos usar la siguiente estructura:

```sh
#!/bin/bash
echo PID: $$
while true
do
  # Grupo de tareas que queremos que se ejecuten.
  sleep <algun tiempo>
done
```

* Si se requiere una respuesta inmediata podemos aprovechar  el comando wait que es asincrónico,

```sh
#!/bin/bash
echo PID: $$
while true
do
  sleep <algun tiempo> & #NOTAR QUE ESTOY CREANDO UN SUBPROCESO
  wait $! #Espero asincronicamente por el subproceso
done
```

* Finalmente, a nuestro demonio nos faltaría poder, obtener una finalización controlada, para ello podemos emplear lo visto en señales

```sh
#!/bin/bash

finalizar()
{
  #codigo    
}

obtenerInfo()
{
  #codigo    
}


trap finalizar() SIGTERM
trap obtenerInfo() SIGUSR1

echo PID: $$
while true
do
  sleep <algun tiempo> & #NOTAR QUE ESTOY CREANDO UN SUBPROCESO
  wait $! #Espero asincronicamente por el subproceso
done
```

# AWK

### Que es?
AWK es un lenguaje de programación diseñado para procesar datos basados en texto, ya sean ficheros o flujos de datos.
Su entrada por default es el stdin y su salida por default es stdout, las mismas pueden ser modificadas al igual que hacemos con todos los comandos hasta ahora.

```sh
<Comando UNIX> | awk '<comando_awk>' 
<Comando UNIX> | awk '<comando_awk>' > <FileOUT>
awk 'comando_awk' <FileIN>
awk 'comando_awk' <FileIN> > <FileOUT>
```

### Comportamiento
Lee la entrada un renglón a la vez, cada renglón se compara con cada patrón en orden; 
para cada padrón que concuerde con el renglón se efectúa la acción correspondiente. 
Si se omite la acción, la acción por defecto consiste en imprimir los renglones que concordaron con el patrón y si se omite el patrón, 
la parte de la acción se hace en cada renglón de entrada. 
Awk divide cada renglón de entrada en campos, (por defecto) cada campo estará separado por espacios, llama a los campos $1, $2, ..$NF donde NF es una variable 
cuyo valor es igual al número de campos. 
Los patrones deben ir rodeados por caracteres / y puede contener dos patrones separados por una coma, 
en cuyo caso la acción se realizará para aquellas líneas comprendidas entre la primera aparición del primer patrón y la siguiente aparición del 
segundo patrón.

### Variables especiales
| Variable | Descripción | 
| -------- | -------- |
| ARGC | Número de argumentos pasados en la línea de comandos.
| ARGV | Arreglo de argumentos de la línea de comandos de awk.
| FILENAME  | Nombre del archivo de entrada actual (file for input).
| RS | Separador de líneas o registros (Record Separator).  Si no se especifica asume por default: NewLine.
| ORS | Separador de líneas o registros en la salida (Output Record Separator). Si no se especifica asume por default: NewLine.
| FS | Separador de campos  (Field Separator). 
| OFS | Separador de campos en salida (Output Field Separator). 
| OFMT | Formato de salida para números (Output ForMaT).
| NR | Número de línea o registro de entrada (Number of record).
| NF | Número de campos de la línea o registro de entrada (Number of Fields).

### Sintaxis mas basica
/patrón/ { acción }

### Ejemplos 
* Supongamos que queremos imprimir solo los 3 primeros procesos que devuelve ps. Notar que la primera fila de PS son los encabezados. [start,end]


```sh
ps -all | awk 'NR==2,NR==4'
```
* Supongamos ahora que queremos copiar estas lineas a un nuevo archivo. $0 es la fila entera.

```sh
ps -all | awk 'NR==2,NR==4 { print NR, $0 }' > file
```
* Contador de palabras.

```sh
awk '{ nw+=NF } END { print nw}'
```
