#!/bin/bash

function signal_SIGUSR1
{
    echo "RX=>USR1."
}

function signal_SIGUSR2
{
    echo "RX=>USR2. SIGTERM HABILITADA"
    trap SIGTERM
}

echo "PID: $$"

# Conecta las señales con las funciones.
trap signal_SIGUSR1 SIGUSR1 SIGSYS
trap signal_SIGUSR2 SIGUSR2

# Ignora la señal SIGTERM
trap '' SIGTERM

# Espera las señales.
echo 'Presione una tecla para terminar'
read