#!/bin/bash
#Los array en bash:
#  No tienen un limite fijo de tamaño (Solo el espacio disponible en la memoria lo limita).
#  Se indexan a partir del 0. 
#  No se almacenan contiguamente en memoria.
#  Son Unidimensionales
#Tipos de array
#Indexados (a)
#Asociativos (A)

#Declaracion
    # Crear un array vacio
    ArrayVacio=()
    # Crear un array en forma explicita
    declare -a ArrayExplicito 
    # Crear un array en forma explicita y inicializado
    declare -a ArrayExpIni=('hola','tu',123.12)
    # Crear un array en forma implicita
    ArrayImplicito=('chau',123,'UNLAM')
    # Convertir una variable en array
    var=2
    var[1]=21
    # Crear un array por copia
    ArrayCopia=("${ArrayExpIni[*]}")
    # Crear un Array inicializado indicando indices
    ArrayIniIndex=([0]=2 [3]=2 [1]=32)

#Agregar valor
array=('adrian' 'radice' 23)
array+=('UNLAM') ##OJO NO OLVIDAR LOS PARENTESIS

#Obtener valor
    # Indices positivos
    for ((i=0;i<4;i++)){
        echo ${array[$i]}
    }
    # Indices negativos
    for ((i=-1;i>=-4;i--)){
        echo ${array[$i]}
    }
    # Expansion en masa
    echo ${array[*]}
    echo ${array[@]}
    echo ${array[@]:1:2}

#Concatenar
array2=("${array[@]}","${array[@]/adrian/daniela}")

#Obtener logitud del array
echo ${#array[@]}

#Obtener indices del array
echo ${!array[*]}

#Liberar espacio array
unset -v array
#Liberar un elemento
unset -v array[1]

#Diferenciando # y *
a=('hola adrian' 'como estas bien')

a2=(${a[*]})
a3=("${a[*]}")
a4=(${a[@]})
a5=("${a[@]}")
